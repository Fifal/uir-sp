package cz.zcu.kiv.uir.fjani.util;

import cz.zcu.kiv.uir.fjani.algorithms.*;
import cz.zcu.kiv.uir.fjani.config.Config;
import cz.zcu.kiv.uir.fjani.model.Vector;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;

import java.awt.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by Filip Jani on 27. 4. 2016.
 */
public final class Util {
    public static ArrayList<Vector> vectorList = new ArrayList<>();

    public static int convertRGB(int RGB) {
        int r = (RGB >> 16) & 0xFF;
        int g = (RGB >> 8) & 0xFF;
        int b = (RGB & 0xFF);
        int grey = (r + g + b) / 3;

        return grey;
    }

    public static boolean isInVectorList(int[] vector) {
        for (Vector v : vectorList) {
            if (Arrays.toString(v.getVector()).equals(Arrays.toString(vector))) {
                return true;
            }
        }
        return false;
    }

    public static void clearVectorList() {
        vectorList.clear();
    }

    public static void addVector(Vector vector) {
        vectorList.add(vector);
    }

    public static void saveVectorsToFile() throws IOException {
        File file = new File("vectors.txt");

        if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter fw = new FileWriter(file.getAbsoluteFile(), false);
        BufferedWriter bw = new BufferedWriter(fw);
        for (int i = 0; i < vectorList.size(); i++) {
            bw.write(vectorList.get(i).toString() + "\n");
        }
        bw.close();

    }

    /**
     * Vrací pole 0 a 1, 0 pokud je v obrázku na pixelu [x,y] bílá barva, 1 pokud je tam černá barva
     *
     * @return array pole nul a jedniček
     */
    public static int[][] getArrayFromImage(Image image) {
        int[][] array = new int[(int) image.getHeight()][(int) image.getWidth()];
        PixelReader pixelReader = image.getPixelReader();
        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                int rgb = Util.convertRGB(pixelReader.getArgb(x, y));
                if (rgb < Config.TOLERATION) {
                    array[y][x] = 1;
                } else array[y][x] = 0;
            }
        }
        return array;
    }


    /**
     * Metoda na thinning pole
     *
     * @param binaryImage pole nul a jedniček
     * @return vythinované pole
     */
    public static int[][] doZhangSuenThinning(int[][] binaryImage) {
        int a, b;
        java.util.List<Point> pointsToChange = new LinkedList<>();
        boolean hasChange;
        do {
            hasChange = false;
            for (int y = 1; y + 1 < binaryImage.length; y++) {
                for (int x = 1; x + 1 < binaryImage[y].length; x++) {
                    a = getA(binaryImage, y, x);
                    b = getB(binaryImage, y, x);
                    if (binaryImage[y][x] == 1 && 2 <= b && b <= 6 && a == 1
                            && (binaryImage[y - 1][x] * binaryImage[y][x + 1] * binaryImage[y + 1][x] == 0)
                            && (binaryImage[y][x + 1] * binaryImage[y + 1][x] * binaryImage[y][x - 1] == 0)) {
                        pointsToChange.add(new Point(x, y));
                        hasChange = true;
                    }
                }
            }
            for (Point point : pointsToChange) {
                binaryImage[(int) point.getY()][(int) point.getX()] = 0;
            }
            pointsToChange.clear();
            for (int y = 1; y + 1 < binaryImage.length; y++) {
                for (int x = 1; x + 1 < binaryImage[y].length; x++) {
                    a = getA(binaryImage, y, x);
                    b = getB(binaryImage, y, x);
                    if (binaryImage[y][x] == 1 && 2 <= b && b <= 6 && a == 1
                            && (binaryImage[y - 1][x] * binaryImage[y][x + 1] * binaryImage[y][x - 1] == 0)
                            && (binaryImage[y - 1][x] * binaryImage[y + 1][x] * binaryImage[y][x - 1] == 0)) {
                        pointsToChange.add(new Point(x, y));
                        hasChange = true;
                    }
                }
            }
            for (Point point : pointsToChange) {
                binaryImage[(int) point.getY()][(int) point.getX()] = 0;
            }
            pointsToChange.clear();
        } while (hasChange);
        return binaryImage;
    }

    private static int getA(int[][] binaryImage, int y, int x) {
        int count = 0;
//p2 p3
        if (y - 1 >= 0 && x + 1 < binaryImage[y].length && binaryImage[y - 1][x] == 0 && binaryImage[y - 1][x + 1] == 1) {
            count++;
        }
//p3 p4
        if (y - 1 >= 0 && x + 1 < binaryImage[y].length && binaryImage[y - 1][x + 1] == 0 && binaryImage[y][x + 1] == 1) {
            count++;
        }
//p4 p5
        if (y + 1 < binaryImage.length && x + 1 < binaryImage[y].length && binaryImage[y][x + 1] == 0 && binaryImage[y + 1][x + 1] == 1) {
            count++;
        }
//p5 p6
        if (y + 1 < binaryImage.length && x + 1 < binaryImage[y].length && binaryImage[y + 1][x + 1] == 0 && binaryImage[y + 1][x] == 1) {
            count++;
        }
//p6 p7
        if (y + 1 < binaryImage.length && x - 1 >= 0 && binaryImage[y + 1][x] == 0 && binaryImage[y + 1][x - 1] == 1) {
            count++;
        }
//p7 p8
        if (y + 1 < binaryImage.length && x - 1 >= 0 && binaryImage[y + 1][x - 1] == 0 && binaryImage[y][x - 1] == 1) {
            count++;
        }
//p8 p9
        if (y - 1 >= 0 && x - 1 >= 0 && binaryImage[y][x - 1] == 0 && binaryImage[y - 1][x - 1] == 1) {
            count++;
        }
//p9 p2
        if (y - 1 >= 0 && x - 1 >= 0 && binaryImage[y - 1][x - 1] == 0 && binaryImage[y - 1][x] == 1) {
            count++;
        }
        return count;
    }

    private static int getB(int[][] binaryImage, int y, int x) {
        return binaryImage[y - 1][x] + binaryImage[y - 1][x + 1] + binaryImage[y][x + 1]
                + binaryImage[y + 1][x + 1] + binaryImage[y + 1][x] + binaryImage[y + 1][x - 1]
                + binaryImage[y][x - 1] + binaryImage[y - 1][x - 1];
    }

    public static String recognize(Image image, String paramAlg, String clasAlg) {
        Map<Double, Integer> map = new HashMap<>();
        int[][] array = Util.getArrayFromImage(image);
        int[] vector;
        switch (paramAlg) {
            case "Fifals":
                array = Util.doZhangSuenThinning(array);
                vector = Fifals.getVector(array);
                break;
            case "BoW":
                vector = BagOfWords.getVector(array);
                break;
            case "FifalsQuad":
                array = Util.doZhangSuenThinning(array);
                vector = FifalsQuad.getVector(array);
                break;
            default:
                array = Util.doZhangSuenThinning(array);
                vector = Fifals.getVector(array);
                break;
        }

        double distanceTotal = Double.MAX_VALUE;
        for (Vector v : Util.vectorList) {
            double distance;
            double total = 0;
            for (int i = 0; i < v.getVector().length - 1; i++) {
                try {
                    total += Math.pow(vector[i] - v.getVector()[i], 2);
                } catch (Exception ex) {
                }
            }
            distance = Math.sqrt(total);
            map.put(distance, v.getNumber());
            if (distance < distanceTotal) {
                distanceTotal = distance;
            }
        }

        int numberResult = -1;
        switch (clasAlg) {
            case "KNN":
                numberResult = KNN.getResult(map);
                break;
            case "SmallestDistance":
                numberResult = SmallestDistance.getResult(map);
                break;

        }
        return Integer.toString(numberResult);
    }
}
