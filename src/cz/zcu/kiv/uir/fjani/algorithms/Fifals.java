package cz.zcu.kiv.uir.fjani.algorithms;

import cz.zcu.kiv.uir.fjani.util.Util;
import cz.zcu.kiv.uir.fjani.model.Vector;
import javafx.scene.image.Image;



/**
 * Třída generující příznakový vektor pro číslo.
 * Výsledný vektor má 256 složek. Prvních 128 složek, je počet 1 v každém sloupci matice.
 * Druhých 128 složek je počet 1 v každém sloupci matice.
 */
public class Fifals {
    public Fifals() {
    }

    /**
     * Vyytvoří příznakový vektor z obrázku a vloží tento vektor do souboru
     * @param image  image obrázek pro který se vektor generuje
     * @param number number číslo pro který se vektor generuje
     */
    public void learn(Image image, int number) {
        int[][] arrray = Util.getArrayFromImage(image);
        arrray = Util.doZhangSuenThinning(arrray);
        int[] vector = getVector(arrray);
        if (!Util.isInVectorList(vector)) {
            Util.addVector(new Vector(number, vector));
        }
        try{
            Util.saveVectorsToFile();
        }
        catch (Exception ex){

        }
    }


    /**
     * Vrací příznakový vektor pro dané pole
     * @param array pole nul a jedniček
     * @return vector příznakový vektor
     */
    public static int[] getVector(int[][] array) {
        int[] vector = new int[128 * 4];
        int k = 0;
        for (int i = 0; i < array.length; i++) {
            int onesCounter = 0;
            for (int j = 0; j < array.length; j++) {
                if (array[j][i] == 1) {
                    onesCounter++;
                }
            }
            vector[k] = onesCounter;
            k++;
        }
        for (int i = 1; i < array.length; i++) {
            int onesCounter = 0;
            for (int j = 0; j < array.length; j++) {
                if (array[i][j] == 1) {
                    onesCounter++;
                }
            }
            vector[k++] = onesCounter;
        }
        for (int i = array.length-1; i >= 0; i--) {
            int count = 0;
            int l = 0;
            for (int j = i; j < 128; j++) {
                count += array[j][l];
                l++;
            }
            vector[k] = count;
            k++;
        }
        for (int i = 0; i < array.length; i++) {
            int count = 0;
            int l = 128-1;
            for (int j = 128-1; j > 0; j--) {
                count += array[j][l];
                l--;
            }
            vector[k] = count;
            k++;
        }
        return vector;
    }

}