package cz.zcu.kiv.uir.fjani.algorithms;

import cz.zcu.kiv.uir.fjani.config.Config;


import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Filip Jani on 3. 5. 2016.
 */
public class SmallestDistance {

    public static int getResult(Map<Double, Integer> map){
        TreeMap<Double, Integer> sorted = new TreeMap<>(map);
        double[] numbers = new double[10];
        for (int i = 0; i < sorted.size(); i++) {
            double value = (double)sorted.keySet().toArray()[i];
            int key = sorted.get(value);
            numbers[key] += value;
        }
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = numbers[i]/Config.NUMBERS_LOADED;
        }
        double smallestDistance = Double.MAX_VALUE;
        int number = -1;
        for (int i = 0; i < numbers.length; i++) {
            if(numbers[i] < smallestDistance){
                smallestDistance = numbers[i];
                number = i;
            }
        }
        return number;
    }

}
